export interface FicheroImagen {
    nombre: string;
    peso: number;
    fichero: File;
    progreso: number;
  }
