import { Component, OnInit, Input } from '@angular/core';
import { ImagenCompleta } from 'src/app/interfaces/ImagenCompleta';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cartel',
  templateUrl: './cartel.component.html',
  styleUrls: ['./cartel.component.css']
})
export class CartelComponent implements OnInit {
  @Input() cartel: ImagenCompleta;
  @Input() id: string;

  carteles: any;
  constructor(private _service: FirebaseService, private router: Router) {}

  ngOnInit() {
    this._service.cargarImagenes().subscribe(res => (this.carteles = res));
  }

  verDetalle(idx: number) {
    let cartel;
    cartel = this.carteles.filter(elem => elem.id === idx);
    this.router.navigate(['/coleccion', cartel[0].id]);
    // console.log(cartel[0].id);
  }
}
