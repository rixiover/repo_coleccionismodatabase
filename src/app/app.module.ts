import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { AboutComponent } from './components/about/about.component';
import { IndividualComponent } from './components/formularios/individual/individual.component';
import { MultipleComponent } from './components/formularios/multiple/multiple.component';
import { BaseCompletaComponent } from './components/contenido/base-completa/base-completa.component';
import { DetalleComponent } from './components/contenido/detalle/detalle.component';
import { ResultadoBusquedaComponent } from './components/contenido/resultado-busqueda/resultado-busqueda.component';
import { PortadaComponent } from './components/portada/portada.component';
import { NuevoComponent } from './components/formularios/nuevo/nuevo.component';
import { CartelComponent } from './components/contenido/cartel/cartel.component';
import { CartelesService } from './services/carteles.service';
import { FirebaseService } from './services/firebase.service';
import { HttpClientModule } from '@angular/common/http';
import { CargarIdentificadoresPipe } from './pipes/cargar-identificadores.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    AboutComponent,
    IndividualComponent,
    MultipleComponent,
    BaseCompletaComponent,
    DetalleComponent,
    ResultadoBusquedaComponent,
    PortadaComponent,
    NuevoComponent,
    CartelComponent,
    CargarIdentificadoresPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFirestoreModule,
    HttpClientModule
  ],
  providers: [CartelesService, FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule {}
