import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cargarIdentificadores'
})
export class CargarIdentificadoresPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    const keys = Object.keys(value);

    keys.forEach( key => { value[key].id = key; });
    return value;
  }

}
