import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { CartelesService } from './carteles.service';
import { FicheroImagen } from '../interfaces/imagen';
import { finalize } from 'rxjs/operators';
import { ImagenCompleta } from '../interfaces/ImagenCompleta';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  private carpeta = 'imagenes';
  public infoCarteles: any;
  public elemSelec = [];
  // public infoCartelIndividual: any;
  constructor(
    private almacenamiento: AngularFireStorage,
    private database: AngularFirestore,
    private _infoCarteles: CartelesService
  ) {}

  cargarInfoCarteles() {
    this._infoCarteles.getCarteles().subscribe(res => {
      return (this.infoCarteles = res);
    });
  }

  cargarImagenes() {
    return this.database.collection(this.carpeta).snapshotChanges();
  }

  cargarImagenesPorId() {
    return this.database
      .collection(this.carpeta)
      .valueChanges()
      .subscribe(res => console.log(res));
  }

  chequearImagen(array, elem) {
    if (elem === undefined) {
      console.log('no imagen');
      return;
    }
    return array.filter(
      person =>
        person.nombre
          .trim()
          .toUpperCase()
          .indexOf(elem.nombre.trim().toUpperCase()) >= 0
    );
  }

  subirRegistroIndividual(registro: any, imagen: any) {
    this.database.collection(this.carpeta).get().subscribe(res => {
      this.infoCarteles = res;
      console.log(registro);

      const idArray = [];

      this.infoCarteles.forEach(element => {
        const id = element.id;
        idArray.push(id);
      });

      const idSelec = Math.max.apply(Math, idArray) + 1;

      const file = imagen[0].fichero;
      const ruta = `${this.carpeta}/${imagen[0].nombre}`;
      const referencia = this.almacenamiento.ref(ruta);
      const tarea = this.almacenamiento.upload(ruta, file);

      tarea
        .percentageChanges()
        .subscribe(progreso => (imagen[0].progreso = progreso));

      tarea
          .snapshotChanges()
          .pipe(
            finalize(() => {
              referencia.getDownloadURL().subscribe(result => {
                this.guardarReferenciaImagen({
                  id: idSelec,
                  url: result,
                  nombre: registro.nombre,
                  grupo: registro.grupo,
                  subgrupo: registro.subgrupo,
                  fecha: registro.fecha,
                  ilustrador: registro.ilustrador,
                  tipoIlustracion: registro.tipoIlustracion,
                  valoración: registro.valoracion
                });
              });
            })
          )
          .subscribe(snapshot => console.log(snapshot));
    });
  }

  subirImagenesAlServidor(imagenes: FicheroImagen[]) {
    this._infoCarteles.getCarteles().subscribe(res => {
      // console.log(res);
      this.infoCarteles = res;
      // console.log(this.infoCarteles);
      imagenes.forEach((elemento, index, array) => {
        const elemSelec = this.chequearImagen(this.infoCarteles, elemento);
        // console.log(elemSelec);

        if (elemSelec.length === 0) {
          // console.log('non');
          return;
        }
        console.log('vamos a por ello');
        const file = elemento.fichero;
        const ruta = `${this.carpeta}/${elemento.nombre}`;
        const referencia = this.almacenamiento.ref(ruta);
        const tarea = this.almacenamiento.upload(ruta, file);

        tarea
          .percentageChanges()
          .subscribe(progreso => (elemento.progreso = progreso));

        tarea
          .snapshotChanges()
          .pipe(
            finalize(() => {
              referencia.getDownloadURL().subscribe(result => {
                this.guardarReferenciaImagen({
                  id: elemSelec[0].id,
                  url: result,
                  nombre: elemSelec[0].nombre,
                  grupo: elemSelec[0].grupo,
                  subgrupo: elemSelec[0].subgrupo,
                  fecha: elemSelec[0].fechaEstreno,
                  ilustrador: elemSelec[0].ilustrador,
                  tipoIlustracion: elemSelec[0].tipoIlustracion,
                  valoración: elemSelec[0].valoracion
                });
              });
            })
          )
          .subscribe(snapshot => console.log(snapshot));
      });
    });
  }

  guardarReferenciaImagen(imagen: ImagenCompleta) {
    this.database
      .collection(this.carpeta)
      .add(imagen)
      .then(res => console.log('Imagen guardada con éxito', imagen.nombre))
      .catch(err => console.log('Ocurrió un error', err, imagen.nombre));
  }
}
